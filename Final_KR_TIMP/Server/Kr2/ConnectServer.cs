﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Kr2
{
    class ConnectServer
    {
        const string ip = "127.0.0.1";
        const int port = 8888;
        public string Data;
        public void Connect()
        {
            var tcpEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            var tcpSockets = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            tcpSockets.Bind(tcpEndPoint);
            tcpSockets.Listen(5);

            while (true)
            {
                var listener = tcpSockets.Accept();

                var buffer = new byte[256];
                var size = 0;
                var data = new StringBuilder();
                do
                {
                    size = listener.Receive(buffer);
                    data.Append(Encoding.UTF8.GetString(buffer, 0, size));
                }
                while (listener.Available > 0);
                Data = data.ToString();




                if ((Data[0] == '1') && (Data[1] == '|') && (Data[2] == '1'))
                {
                    Registration authorization = new Registration();
                    bool check =  authorization.prob(Data);
                    listener.Send(Encoding.UTF8.GetBytes(check.ToString()));
                }
                else if ((Data[0] == '2') && (Data[1] == '|') && (Data[2] == '2'))
                {
                    Auth authorization = new Auth();
                    string ID = "";
                    bool check = authorization.Check(Data);
                    if(check)
                        ID = authorization.Id(Data);
                    listener.Send(Encoding.UTF8.GetBytes(check.ToString()+ID));
                }
                else if ((Data[0] == '3') && (Data[1] == '|'))
                {
                    PhonebookSer phonebook = new PhonebookSer();
                    string message = phonebook.NumFio(Data, Program.DB);
                    listener.Send(Encoding.UTF8.GetBytes(message));
                }
                else if((Data[0] == '4') && (Data[1] == '|'))
                {
                    Notesser zametki = new Notesser();
                    string message = zametki.nots(Data, Program.DB);
                    listener.Send(Encoding.UTF8.GetBytes(message));
                }
                else if ((Data[0] == '5') && (Data[1] == '|'))
                {
                    Diaryser diary = new Diaryser();
                    string message = diary.Diary(Data, Program.DB);
                    listener.Send(Encoding.UTF8.GetBytes(message));
                }

                listener.Shutdown(SocketShutdown.Both);
                listener.Close();
            }
        }
    }
}
