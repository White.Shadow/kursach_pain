﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kr2
{
    public class Notesser
    {
        public string nots(string Data, SQLiteConnection db)
        {
            SQLiteCommand command = db.CreateCommand();
            string Zagl = "", Txt = "", message = "0", id = "";
            int K = 0;
            for (int i = 3; i < Data.Length; i++)
            {
                if (Data[i] == '|')
                {
                    K++;
                }
                else if (K == 0)
                {
                    id += Data[i];
                }
                else if (K == 1)
                    Zagl += Data[i];
                else Txt += Data[i];
            }


            if (Data[2] == '3')
            {
                command.Parameters.Add("@Zagl", DbType.String).Value = Zagl.ToUpper();
                command.Parameters.Add("@Txt", DbType.String).Value = Txt;
                command.Parameters.Add("@Id", DbType.String).Value = id;
                //command.CommandText = "select * from Телефоны where Номер like @Nomer";
                command.CommandText = "insert into Заметки(Id, Заголовки, Текст) values(@Id, @Zagl, @Txt)";
                command.ExecuteNonQuery();
            }
            else if (Data[2] == '4')
            {
                message = "";
                command.CommandText = "select * from Заметки where  Заголовки like @Zagl and Id like @Id ";
                command.Parameters.Add("@Zagl", DbType.String).Value = Zagl.ToUpper();
                command.Parameters.Add("@Id", DbType.String).Value = id;
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        message += reader["Текст"];
                    }

                }
                else message = "false";
            }
            else if (Data[2] == '5') 
            {
                message = "";
                command.CommandText = "select * from Заметки where Id like @Id ";
                command.Parameters.Add("@Id", DbType.String).Value = id;
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        message += reader["Заголовки"] + "\r\n";
                    }
                }
            }
            return message;
        }
    }
}
