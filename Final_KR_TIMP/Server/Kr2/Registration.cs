﻿using System;
using System.Data;
using System.Data.SQLite;

namespace Kr2
{
    class Registration
    {
        public bool prob(string Data)
        {
            SQLiteCommand command = Program.DB.CreateCommand();
            bool check;
            string log = "", passw = "";
            int K = 0;
            for (int i = 3; i < Data.Length - 1; i++)
            {
                if (Data[i] == '|')
                {
                    K = 1;
                }
                if (K == 0)
                    log += Data[i];
                else passw += Data[i + 1];

            }
            command.Parameters.Add("@log", DbType.String).Value = log;
            command.Parameters.Add("@passwd", DbType.String).Value = passw;
            command.CommandText = "select * from Авторизация where Логин like @log";
            if (command.ExecuteScalar() == null)
            {
                command.CommandText = "insert into Авторизация(Логин, Пароль) values(@log, @passwd)";
                check = true;
            }
            else check = false;
            command.ExecuteNonQuery();
            return check;
        }
    }
}
