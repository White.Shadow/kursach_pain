﻿using System;
using System.Windows.Forms;

namespace Communication
{
    public partial class Вход : Form
    {

        public Вход()
        {
            InitializeComponent();
        }
        ConnectClient connectClient = new ConnectClient();

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new Registration();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void Input_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != "" && textBox2.Text != "")
                {
                    string Message = connectClient.ConServer("2|2" + textBox1.Text + "|" + textBox2.Text);

                    if (Message[0] == 'T')
                    {
                        string id = "";
                        for (int i = 4; i < Message.Length; i++)
                        {
                            id += Message[i];
                        }
                        this.Hide();
                        var formMain = new selection(id);
                        formMain.Closed += (s, args) => this.Close();
                        formMain.Show();

                    }
                    else
                    {
                        MessageBox.Show("Вы ввели неправильный Логин или Пароль!");
                    }
                }
                else MessageBox.Show("Логин или пароль не может быть пустым!");
            }
            catch
            {
                MessageBox.Show("Сервер не запущен!");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }
        }
    }
}
