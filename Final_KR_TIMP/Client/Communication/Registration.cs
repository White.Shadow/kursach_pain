﻿using System;
using System.Windows.Forms;

namespace Communication
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }

        ConnectClient connectClient = new ConnectClient();

        private void Auth_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxlog.Text != "" && textBoxpasswd.Text != "")
                {
                    string Message = connectClient.ConServer("1|1" + textBoxlog.Text + "|" + textBoxpasswd.Text);
                    if (Message[0] == 'T')
                    {
                        MessageBox.Show("Регистрация завершена!");
                    }
                    else MessageBox.Show("Такой пользователь уже существует!");
                }
                else MessageBox.Show("Логин или пароль не может быть пустым!");
            }
            catch
            {
                MessageBox.Show("Сервер не запущен");
            }
            }
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new Вход();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new Вход();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void Registration_Load(object sender, EventArgs e)
        {
            textBoxpasswd.MaxLength = 20;
        }

        private void textBoxpasswd_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void textBoxpasswd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|') || (textBoxpasswd.TextLength >= 20) && (e.KeyChar != 8))
            {
                e.Handled = true;
            }
        }

        private void textBoxlog_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }

        }

        private void textBoxlog_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
