﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Communication
{
    class ConnectClient
    {
        const string ip = "127.0.0.1";
        const int port = 8888;
        public string ConServer(string text)
        {
            var tcpEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            var tcpSockets = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // Console.WriteLine("Введите сообщение:");
            var Message = text;
            var data = Encoding.UTF8.GetBytes(Message);
            tcpSockets.Connect(tcpEndPoint);
            tcpSockets.Send(data);

            var buffer = new byte[256];
            var size = 0;
            var answer = new StringBuilder();

            do
            {
                size = tcpSockets.Receive(buffer);
                answer.Append(Encoding.UTF8.GetString(buffer, 0, size));
            }
            while (tcpSockets.Available > 0);

            tcpSockets.Shutdown(SocketShutdown.Both);
            tcpSockets.Close();
            return answer.ToString();
        }
    }
}
