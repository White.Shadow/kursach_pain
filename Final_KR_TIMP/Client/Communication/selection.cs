﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Communication
{
    public partial class selection : Form
    {
        string Id;
        public selection(string id)
        {
            InitializeComponent();
             Id = id;
        }
        private void button1_Click(object sender, EventArgs e)
        {
           // this.Hide();
            var formMain = new Data_time();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //this.Hide();
            var formMain = new Phonebook(Id);
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
           // this.Hide();
            var formMain = new Notes(Id);
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void selection_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            // this.Hide();
            var formMain = new Diary(Id);
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }
    }
}
